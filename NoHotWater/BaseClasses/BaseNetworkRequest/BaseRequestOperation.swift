//
//  BaseRequestOperation.swift
//  NoHotWater
//
//  Created by Александр Волков on 27.10.2020.
//

import Foundation
import Alamofire
import RxSwift

protocol RequestOperationProtocol {
    associatedtype ResponseModel: ServerResponseModelProtocol
    var inputParameters: [String:String]? { get }
    var urlParameter: String? { get }
    var request: RequestProtocol { get }
    func execute(in dispatcher: NetworkDispatcherProtocol, with completion: @escaping (Result<ResponseModel.Output>)->Void) throws
}

extension RequestOperationProtocol {
 
    func execute(in dispatcher: NetworkDispatcherProtocol, with completion: @escaping (Result<ResponseModel.Output>)->Void) throws {
        try dispatcher.execute(request: self.request) { response in
            let checkedResult = dispatcher.errorHandler.handle(result: response.result, with: response.response?.statusCode)
            
            switch checkedResult {
            case .success(let data):
                
                guard let jsonData = try? JSONSerialization.data(withJSONObject: data, options: .prettyPrinted) else {
                    completion(Result.failure(DomainErrors.unknownError(description: "Bad response, unhadled error")))
                    return
                }
                
                do {
                    let decoder = JSONDecoder()
                    decoder.dateDecodingStrategy = .iso8601
                    let responseModel = try decoder.decode(ResponseModel.self, from: jsonData)
                    completion(Result.success(responseModel.data))
                } catch {
                    completion(Result.failure(DomainErrors.unknownError(description: "Bad response format, unhadled error")))
                    return
                }
                
            case .failure(let error):
                completion(Result.failure(error))
                break
            }
        }
    }
    
}

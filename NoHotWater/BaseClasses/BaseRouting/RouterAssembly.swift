//
//  RouterAssembly.swift
//  NoHotWater
//
//  Created by Александр Волков on 27.10.2020.
//

import UIKit

struct RouterDependencies {
    let routerAssembly: RouterAssemblyProtocol
    let classifierService: ClassifierServiceProtocol
}

protocol RouterAssemblyProtocol {
    
    func assembleMainScreen(on navigation: UINavigationController?, with dependencies: RouterDependencies)
    func assembleMainScreenAsRoot(with dependencies: RouterDependencies)
}

class RouterAssembly: NSObject, RouterAssemblyProtocol {

    func assembleMainScreen(on navigation: UINavigationController?, with dependencies: RouterDependencies) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "CHPViewController") as! CHPViewController
        let module = CHPAssembly()
        module.createModule(with: vc, dependencies: dependencies)
        navigation?.setViewControllers([vc], animated: false)
    }

    func assembleMainScreenAsRoot(with dependencies: RouterDependencies) {
        let rootNavController = UIApplication.shared.windows.first?.rootViewController as! UINavigationController
        assembleMainScreen(on: rootNavController, with: dependencies)
    }
    
    func pushUserScreen(on navigation: UINavigationController?, with dependencies: RouterDependencies) {
    }
    
    
}


//
//  Classifier.swift
//  NoHotWater
//
//  Created by Александр Волков on 27.10.2020.
//

import Foundation

protocol ClassifierProtocol {

    var classifierId: Int { get }
    var classifierName: String { get }
    var file: String { get }
    var CHPs: [CHP]? { get }
    var version: Int { get }
}

struct Classifier: ClassifierProtocol, Codable {

    var classifierId: Int
    var classifierName: String
    var file: String
    var CHPs: [CHP]?
    var version: Int
    
    
    enum CodingKeys: String, CodingKey {
        case classifierId
        case classifierName
        case file
        case version
    }
    
    init(from decoder: Decoder) throws {
        let map = try decoder.container(keyedBy: Classifier.CodingKeys.self)
        file = try map.decode(String.self, forKey: .file)
        CHPs = CHPDataService.shared.getCHPList(from: file)
        classifierId = try map.decode(Int.self, forKey: .classifierId)
        classifierName = try map.decode(String.self, forKey: .classifierName)
        version = try map.decode(Int.self, forKey: .version)
    }
}


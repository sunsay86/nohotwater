//
//  ClassifiersResponseModel.swift
//  NoHotWater
//
//  Created by Александр Волков on 28.10.2020.
//
import Foundation

protocol ClassifiersResponseModelProtocol {
    var data: ClassifiersResponseDataModel { get }
    var status: String { get }
    var expectedResponseDate: String { get }
}

struct ClassifiersResponseModel: ServerResponseModelProtocol {
    
    var data: ClassifiersResponseDataModel
    var status: String
    var expectedResponseDate: String
    
    enum CodingKeys: String, CodingKey {
        case status
        case data = "responseData"
        case expectedResponseDate
    }
    
    init(from decoder: Decoder) throws {
        let map = try decoder.container(keyedBy: ClassifiersResponseModel.CodingKeys.self)
        status = try map.decode(String.self, forKey: .status)
        data = try map.decode(ClassifiersResponseDataModel.self, forKey: .data)
        expectedResponseDate = try map.decode(String.self, forKey: .expectedResponseDate)
    }
} 

//
//  CHP.swift
//  NoHotWater
//
//  Created by Александр Волков on 27.10.2020.
//

import Foundation

protocol CHPProtocol {
    
    var city: String { get }
    var street: String { get }
    var houseNumber: String { get }
    var building: String? { get }
    var symbol: String? { get }
    var periodOfTurningOff: String { get }
}

struct CHP: CHPProtocol, Codable {

    // MARK: - Internal Properties -
    
    let city: String
    let street: String
    let houseNumber: String
    let building: String?
    var symbol: String?
    var periodOfTurningOff: String
    
}

extension CHP {
    
    // MARK: - Internal Types -
    
    enum CodingKeys: String, CodingKey {
        case city = "Населенный пункт"
        case street = "Адрес жилого здания"
        case houseNumber = "№ дома"
        case building = "корпус"
        case symbol = "литер"
        case periodOfTurningOff = "Период отключения ГВС"
    }
    
}

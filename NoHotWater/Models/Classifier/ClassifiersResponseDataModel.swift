//
//  ClassifiersResponseDataModel.swift
//  NoHotWater
//
//  Created by Александр Волков on 27.10.2020.
//

import Foundation

protocol ClassifiersResponseDataModelProtocol {
    var classifiers: [Classifier] { get }
}

struct ClassifiersResponseDataModel: ClassifiersResponseDataModelProtocol, Codable {
    var classifiers: [Classifier]

    enum CodingKeys: String, CodingKey {
        case classifiers
    }
    
    init(from decoder: Decoder) throws {
        let map = try decoder.container(keyedBy: ClassifiersResponseDataModel.CodingKeys.self)
        classifiers = try map.decode([Classifier].self, forKey: .classifiers)
    }
}

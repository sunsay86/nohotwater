//
//  ClassifiersRequest.swift
//  NoHotWater
//
//  Created by Александр Волков on 27.10.2020.
//

import Foundation

class ClassifiersRequestOperation: RequestOperationProtocol {

    init(query parameters:[String: String]?, urlParameter: String?) {
        self.inputParameters = parameters
    }
    var inputParameters: [String : String]?
    var urlParameter: String?
    typealias ResponseModel = ClassifiersResponseModel
    
    var request: RequestProtocol {
        let request = ClassifiersRequest(query: self.inputParameters, urlParameter: self.urlParameter)
        return request
    }
    
}

class ClassifiersRequest: RequestProtocol {
    required init(query parameters: [String: String]?, urlParameter: String?) {
        self.urlParameter = urlParameter
        self.externalParameters = parameters
    }
    
    var path: String {
        return "UniversalMobileService/classifiers/downloadClassifiers"
    }

    var dataType: DataType {
        return .JSON
    }

    var method: HTTPMethodType {
        return .get
    }

    var staticParameters: [String : String]?
    var externalParameters: [String : String]?
    var urlParameter: String?
    
    
}

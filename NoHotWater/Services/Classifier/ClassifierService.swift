//
//  ClassifierService.swift
//  NoHotWater
//
//  Created by Александр Волков on 27.10.2020.
//

import RxSwift

protocol ClassifierServiceProtocol {

    var classifier: ClassifierProtocol? { get set }
    
    func getClassifierSignal(keyword: String?) -> Observable<ClassifiersResponseModelProtocol>
}

class ClassifierService: ClassifierServiceProtocol {
    
    var classifier: ClassifierProtocol?

    private let dispatcher: NetworkDispatcherProtocol

    init(with dispatcher: NetworkDispatcherProtocol) {
        self.dispatcher = dispatcher
    }

    func getClassifierSignal(keyword: String?) -> Observable<ClassifiersResponseModelProtocol> {
        let signal = Observable<ClassifiersResponseModelProtocol>.create() { observer -> Disposable in
            if let keyword = keyword, !keyword.isEmpty {
                let query = ["classifiersId": keyword]
                let classifierRequest = ClassifiersRequestOperation(query: query, urlParameter: nil)
                do {
                    try classifierRequest.execute(in: self.dispatcher) { result in
                        switch (result) {
                        case .success(let data):
                            observer.on(.next(data))
                            observer.onCompleted()
                        case .failure(let error):
                            observer.onError(error)
                        }
                    }
                } catch let error {
                    observer.onError(error)
                }
            } 
            return Disposables.create()
        }
        return signal
    }
}

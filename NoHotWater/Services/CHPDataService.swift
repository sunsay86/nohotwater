//
//  CHPDataService.swift
//  NoHotWater
//
//  Created by Александр Волков on 05.11.2020.
//

import Foundation
import Zip
import RxSwift

protocol CHPDataServiceProtocol {
    
    func getCHPList(from file: String) -> [CHP]?
}

struct CHPDataService: CHPDataServiceProtocol {
    
    static let shared = CHPDataService()
    
    private init() { }
    
    func getCHPList(from file: String) -> [CHP]? {
        var CHPs: [CHP]?
        let edata: String = file.replacingOccurrences(of: "-", with: "+").replacingOccurrences(of: "_", with: "/")
        guard let data = Data(base64Encoded: edata, options: .ignoreUnknownCharacters) else { return nil }
        let filemanager = FileManager.default
        let path = try! filemanager.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: true)
        let url = path.appendingPathComponent("file"+".zip")
        do {
            try data.write(to: url)
        } catch {
            print("Error while writing: "+error.localizedDescription)
        }
        do {
            let unzipDirectory = try Zip.quickUnzipFile(url)
            print("Unzipped")
            do {
                let fileURLList = try filemanager.contentsOfDirectory(at: unzipDirectory, includingPropertiesForKeys: [], options: [])
                for fileURL in fileURLList {
                    print(fileURL.lastPathComponent)
                    print(fileURL.relativeString)
                    do {
                        let data = try Data(contentsOf: fileURL, options: [.dataReadingMapped, .uncached])
                        let decoder = JSONDecoder()
                        decoder.dateDecodingStrategy = .iso8601
                        CHPs = try decoder.decode([CHP].self, from: data)
                    } catch let error as NSError {
                        print("Error: \(error.localizedDescription)")
                    }
                }
            } catch let error {
                print("Error: \(error.localizedDescription)")
            }
        } catch let error as NSError {
            print("Error while unzipping: "+error.localizedDescription)
        }
        return CHPs
    }
}

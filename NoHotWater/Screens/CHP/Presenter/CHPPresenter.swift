//
//  CHPPresenter.swift
//  NoHotWater
//
//  Created by Александр Волков on 28.10.2020.
//

protocol CHPModuleInputProtocol {
    
}

class CHPPresenter: CHPModuleInputProtocol, CHPViewOutputProtocol, CHPInteractorOutputProtocol {
    
    weak var view: CHPViewInputProtocol?
    let interactor: CHPInteractorInputProtocol
    let router: CHPRouterInputProtocol
    var elements: [[CHPCellViewModelProtocol]] = [[CHPCellViewModelProtocol]]()
    var rawData: [ClassifierProtocol]? = [ClassifierProtocol]()
    private var previousKeyword: String?
    
    init(with interactor: CHPInteractorInputProtocol, router: CHPRouterInputProtocol) {
        self.interactor = interactor
        self.router = router
        
        interactor.configure(with: self)
    }
    
    func configure(with view: CHPViewInputProtocol) {
        self.view = view
        view.config(with: self)
    }
    
    func viewIsReady() {
    }
    
    ///clear state on keyword change
    private func clearState() {
        rawData = [ClassifierProtocol]()
        elements = [[CHPCellViewModelProtocol]]()
    }
    
    func refreshData(keyword: String?) {
        if let prev = previousKeyword, prev != keyword {
            clearState()
        } else if previousKeyword == nil {
            previousKeyword = keyword
            interactor.startDataLoading(keyword: keyword)
        }
    }
    
    func didFailLoadData(with error: Error) {
        self.view?.showFailureRequestAlert(with: error.localizedDescription, actionHandler: nil, completion: nil)
    }
    
    func didLoadData(with classifier: ClassifierProtocol?) {
        print("classifier: \(String(describing: classifier))")
        guard let classification = classifier,
              let items = classification.CHPs else {
            return
        }
        
        let section = 0
        
        if let raw = rawData, raw.indices.contains(section) {
            rawData?[section] = classification
        } else {
            rawData?.append(classification)
        }
        
        let elementsArray = items.map { CHPCellViewModel(with: $0) }
        if elementsArray.count > 0 {
            if elements.count == 0 {
                elements.append(elementsArray)
            } else {
                elements[0] = elementsArray
            }
        }
        
        view?.hideHUD()
        view?.refreshUI()
    }
}

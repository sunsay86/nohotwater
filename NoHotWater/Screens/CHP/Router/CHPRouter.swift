//
//  CHPRouter.swift
//  NoHotWater
//
//  Created by Александр Волков on 28.10.2020.
//

import UIKit

protocol CHPRouterInputProtocol {

    func showDetails(for model:CHPProtocol)

}

class CHPRouter: CHPRouterInputProtocol {
    
    enum CHPRoutes: String {
        case CHPDetailsViewController = "CHPDetailsViewController"
    }

    var dependencies: RouterDependencies
    let view: CHPViewInputProtocol

    init(with dependencies: RouterDependencies, view: CHPViewInputProtocol) {
        self.dependencies = dependencies
        self.view = view
    }
    
    func showDetails(for model: CHPProtocol) {
    }
}


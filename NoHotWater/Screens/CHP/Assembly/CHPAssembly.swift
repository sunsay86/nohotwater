//
//  CHPAssembly.swift
//  NoHotWater
//
//  Created by Александр Волков on 28.10.2020.
//
protocol CHPAssemblyProtocol {

     func createModule(with view: CHPViewInputProtocol, dependencies: RouterDependencies)
    
}
class CHPAssembly {

    func createModule(with view: CHPViewInputProtocol, dependencies: RouterDependencies) {
        let interactor = CHPInteractor(with: dependencies.classifierService)
        let router = CHPRouter(with: dependencies, view: view)
        let presenter = CHPPresenter(with: interactor, router: router)
        presenter.configure(with: view)
    }

}

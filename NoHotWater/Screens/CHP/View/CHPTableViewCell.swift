//
//  CHPTableViewCell.swift
//  NoHotWater
//
//  Created by Александр Волков on 27.10.2020.
//

import UIKit
import SDWebImage

class CHPTableViewCell: UITableViewCell {

    @IBOutlet weak var cityLabel: UILabel!
    @IBOutlet weak var streetLabel: UILabel!
    @IBOutlet weak var houseNumberLabel: UILabel!
    @IBOutlet weak var buildingLabel: UILabel!
    @IBOutlet weak var symbolLabel: UILabel!
    @IBOutlet weak var startDateLabel: UILabel!
    @IBOutlet weak var finishDateLabel: UILabel!
    
    func configure(with viewModel: CHPCellViewModelProtocol) {
        cityLabel.text = viewModel.city
        streetLabel.text = viewModel.street
        houseNumberLabel.text = viewModel.houseNumber
        buildingLabel.text = viewModel.building
        symbolLabel.text = viewModel.symbol
        startDateLabel.text = viewModel.periodOfTurningOff
        finishDateLabel.text = viewModel.periodOfTurningOff
        
    }

    override func prepareForReuse() {
        super.prepareForReuse()
        cityLabel.text = "<City>"
        streetLabel.text = "<Street>"
        houseNumberLabel.text = "<0>"
        buildingLabel.text = "<0>"
        symbolLabel.text = "<А>"
        startDateLabel.text = "<1 января 1970>"
        finishDateLabel.text = "<1 января 2070>"
    }

}

//
//  CHPViewController.swift
//  NoHotWater
//
//  Created by Александр Волков on 28.10.2020.
//

import UIKit

protocol CHPViewInputProtocol: class, ShowActivityController, ShowAlertController {

    var navigationController: UINavigationController? { get }

    func config(with output: CHPViewOutputProtocol)
    func refreshUI()

}

protocol CHPViewOutputProtocol: class {
    
    var elements: [[CHPCellViewModelProtocol]] { get }
    func viewIsReady()
    func refreshData(keyword: String?)
}

class CHPViewController: BaseViewController, CHPViewInputProtocol {
    
    let keyword = "4"

    enum CellIdentifiers: String {
        case CHP = "CHPTableViewCell"
    }

    @IBOutlet weak var tableView: UITableView!
   // MARK: - Private properties -

    private(set) var output: CHPViewOutputProtocol!

    // MARK: - Configuration -

    func config(with output: CHPViewOutputProtocol) {
        self.output = output
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = "График остановок ТЭЦ ТГК-1 2016г"
        output.viewIsReady()
    }

    @objc func refreshView() {
        output.refreshData(keyword: self.keyword)
        showHUD()
    }

    func refreshUI() {
        tableView.reloadData()
    }

}

extension CHPViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return output.elements.count > section ? output.elements[section].count : 0
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return output.elements.count
    }
                   
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifiers.CHP.rawValue) as! CHPTableViewCell
        cell.configure(with: output.elements[indexPath.section][indexPath.row])
        return cell
    }

}

extension CHPViewController: UITableViewDelegate {

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }

}


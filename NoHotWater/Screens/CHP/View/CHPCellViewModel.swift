//
//  CHPCellViewModel.swift
//  NoHotWater
//
//  Created by Александр Волков on 28.10.2020.
//

import Foundation
import UIKit

protocol CHPCellViewModelProtocol {
    var city: String { get }
    var street: String { get }
    var houseNumber: String { get }
    var building: String? { get }
    var symbol: String? { get }
    var periodOfTurningOff: String { get }
}

class CHPCellViewModel: CHPCellViewModelProtocol {
    var city: String
    var street: String
    var houseNumber: String
    var building: String?
    var symbol: String?
    var periodOfTurningOff: String
    
    init(with CHP: CHPProtocol) {
        city = CHP.city
        street = CHP.street
        houseNumber = CHP.houseNumber
        building = CHP.building ?? ""
        symbol = CHP.symbol ?? ""
        periodOfTurningOff = CHP.periodOfTurningOff
    }

}

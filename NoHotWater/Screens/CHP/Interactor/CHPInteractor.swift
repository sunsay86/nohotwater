//
//  CHPInteractor.swift
//  NoHotWater
//
//  Created by Александр Волков on 28.10.2020.
//

import RxSwift

protocol CHPInteractorInputProtocol: class {
    func configure(with output: CHPInteractorOutputProtocol)
    func startDataLoading(keyword: String?)
}

protocol CHPInteractorOutputProtocol: class {
    func didLoadData(with classifier: ClassifierProtocol?)
    func didFailLoadData(with error: Error)
}

class CHPInteractor: CHPInteractorInputProtocol {

    weak var output: CHPInteractorOutputProtocol?
    private let classifierService: ClassifierServiceProtocol
    private let bag = DisposeBag()

    init(with classifier: ClassifierServiceProtocol) {
        self.classifierService = classifier
    }

    func configure(with output: CHPInteractorOutputProtocol) {
        self.output = output
    }
    
    func startDataLoading(keyword: String?) {
        classifierService.getClassifierSignal(keyword: keyword)
            .subscribe(onNext: { data in
                self.output?.didLoadData(with: data)
            }, onError: { [weak self] in
                self?.output?.didFailLoadData(with: $0)
            }).disposed(by: bag)
        

    }
}


//
//  AppDelegate.swift
//  NoHotWater
//
//  Created by Александр Волков on 26.10.2020.
//

import UIKit
import CoreData

import Foundation

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    lazy private var dependencies: RouterDependencies = {
        return loadDependencies()
    }()

    func application(_ application: UIApplication,
                     didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        dependencies.routerAssembly.assembleMainScreenAsRoot(with: dependencies)
        return true
    }

    func loadDependencies() -> RouterDependencies {
        let env                = Environment(.PROD)
        let errorHandler       = NetworkErrorHandler()
        let networkDispatcher  = NetworkDispatcher(environment: env, errorHandler: errorHandler)
        let classiffierService = ClassifierService(with: networkDispatcher)
        let routerAssembly     = RouterAssembly()

        return RouterDependencies(
            routerAssembly: routerAssembly,
            classifierService: classiffierService
        )
    }

}

//
//  Environment.swift
//  NoHotWater
//
//  Created by Александр Волков on 27.10.2020.
//

import Foundation

enum EnvironmentType : String, Decodable {
    case PROD
    
    func host() -> String {
        switch self {
        case .PROD:
            return "https://api.gu.spb.ru"
        }
    }
}

protocol EnvironmentProtocol {
    
    var type: EnvironmentType { get }
    var host: String { get }
    var contentType: String { get }
    var headers: [String: Any] { get set }
    var cachePolicy: URLRequest.CachePolicy { get set }

}

struct Environment: EnvironmentProtocol {

    var headers: [String: Any] = [:]
    var cachePolicy: URLRequest.CachePolicy = .reloadIgnoringLocalAndRemoteCacheData
    let type: EnvironmentType
    let contentType: String = "application/json"
    
    var host: String {
        return type.host()
    }
    
    public init(_ type: EnvironmentType) {
        self.type = type
    }

}

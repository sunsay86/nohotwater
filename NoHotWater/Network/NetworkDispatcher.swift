//
//  NetworkDispatcher.swift
//  NoHotWater
//
//  Created by Александр Волков on 27.10.2020.
//

import Foundation
import Alamofire
import RxSwift

protocol NetworkDispatcherProtocol{
    
    var errorHandler: NetworkErrorHandlerProtocol { get }
    func execute(request: RequestProtocol, completion: @escaping (DataResponse<Any>)->()) throws
    
}

class NetworkDispatcher: NetworkDispatcherProtocol {
    
    public let errorHandler: NetworkErrorHandlerProtocol
    private var environment: EnvironmentProtocol
    
    required init(environment: EnvironmentProtocol, errorHandler: NetworkErrorHandlerProtocol) {
        self.environment = environment
        self.errorHandler = errorHandler
    }
    
    public func execute(request: RequestProtocol, completion: @escaping (DataResponse<Any>)->()) throws {
        let rq = try self.prepareURLRequest(for: request)
        runInBackground({
            let taskId = $0
            print("sending request with url: \(rq.url?.absoluteString ?? "(none)")")
            Alamofire.request(rq).responseJSON(completionHandler: {
                print("got response from request: \(rq.url?.absoluteString ?? "(none)")\nresponse:\n\n\($0 as AnyObject)")
                completion($0)
                UIApplication.shared.endBackgroundTask(taskId)
            })
        })
    }
    
    private func runInBackground(_ closure: @escaping (UIBackgroundTaskIdentifier) -> Void, expirationHandler: (() -> Void)? = nil) {
        DispatchQueue.main.async {
            let taskID: UIBackgroundTaskIdentifier
            if let expirationHandler = expirationHandler {
                taskID = UIApplication.shared.beginBackgroundTask(expirationHandler: expirationHandler)
            } else {
                taskID = UIApplication.shared.beginBackgroundTask(expirationHandler: { })
            }
            closure(taskID)
        }
    }
    
    private func prepareURLRequest(for request: RequestProtocol) throws -> URLRequest {
        let fullUrl = "\(environment.host)/\(request.path)"
        guard let url = URL(string: fullUrl) else {
            throw NetworkErrors.badInput
        }

        var urlRequest = URLRequest(url: url)
        urlRequest.addValue("\(environment.contentType)", forHTTPHeaderField: "Content-Type")
        urlRequest.addValue("\(environment.contentType)", forHTTPHeaderField: "Accept")
        
        var queryParams: [URLQueryItem] = [URLQueryItem]()
        
        if let params = request.staticParameters {
            params.forEach({ (key, value) in
                queryParams.append(URLQueryItem(name: key, value: value))
            })
        }
        
        if let params = request.externalParameters {
            params.forEach({ (key, value) in
                queryParams.append(URLQueryItem(name: key, value: value))
            })
        }
        
        guard var components = URLComponents(string: fullUrl) else {
            throw NetworkErrors.badInput
        }
        
        if !queryParams.isEmpty {
            components.queryItems = queryParams
        }
        
        urlRequest.url = components.url
        components.queryItems = components.queryItems
        
        environment.headers.forEach { urlRequest.addValue($0.value as! String, forHTTPHeaderField: $0.key) }
        
        urlRequest.httpMethod = request.method.rawValue
        
        return urlRequest
    }
}

//
//  Request.swift
//  NoHotWater
//
//  Created by Александр Волков on 27.10.2020.
//

public enum DataType {
    case JSON
}

public enum HTTPMethodType: String {
    case get = "GET"
}

public protocol RequestProtocol {
    
    var path: String { get }
    var method: HTTPMethodType { get }
    var urlParameter: String? { get }
    var externalParameters: [String:String]? { get }
    var staticParameters: [String:String]? { get }
    var dataType: DataType { get }
    
}

protocol ServerResponseModelProtocol: Codable {

    associatedtype Output: Codable

    var data: Output { get }
    
}

